#!/usr/bin/env python3

import asyncio
import argparse
import time
from pathlib import Path
import curses

parser = argparse.ArgumentParser(description='Retrieve a binary stream from serial through ser2net.')

parser.add_argument('command', type=str,
                    help='The command to retrieve the file from the device.')
parser.add_argument('output_file', type=str, metavar="<output filename>",
                    help='Filename to output the results to in the current directory.')
parser.add_argument('--remove-echo', default=False, action='store_true',
                    help='Remove command from initial output if that is echoed by the device.')


args = parser.parse_args()
command = args.command
outfile = args.output_file
remove_echo = args.remove_echo


async def download_from_serial(command, outfile):
    BUFFER_SIZE = int(115200/8)  # Baud to bytes
    printdata = ""

    try:
        reader, writer = await asyncio.open_connection('localhost', 8000)
    except ConnectionRefusedError:
        print("[!] Connection refused. Is ser2net running?")
        return

    print(f'[*] Going to retrieve binary stream using command {command}...')
    command += "\n\r"  # Add a newline and carriage return to 'enter' the command

    # The device might echo the command, so that needs to be removed from the first buffer
    commandlen = len(command.encode())

    # Send the cat command to the device
    writer.write(command.encode())

    firstbuffer = True
    totalsize = 0
    start_time = time.time()

    # Delete the file if it exists
    if Path(outfile).is_file():
        Path(outfile).unlink()

    outfile_obj = open(outfile, 'ab')

    scr = curses.initscr()

    while True:
        # Give the reader time to fill the buffer.
        time.sleep(1)
        try:
            # If we don't receive anything for more than 2 seconds, assume we're done
            data = await asyncio.wait_for(reader.read(BUFFER_SIZE), timeout=2)
        except asyncio.TimeoutError:
            break

        if firstbuffer:
            firstbuffer = False
            if remove_echo:
                data = data[commandlen + 1:]

        outfile_obj.write(data)

        totalsize += len(data)
        time_taken = int(time.time() - start_time)

        decodeddata = data.decode('utf-8', errors="ignore")

        if data:
            printdata += decodeddata + "\n"  # Add latest retrieved data, if any

        try:
            scr.clear()
            scr.addstr(0, 0, f'[*] Received {len(data)} bytes, for a total of {totalsize} bytes - {time_taken}s - {round(totalsize / 1000 / time_taken, 3)} kB/s...')
            lastlines = printdata.split("\n")[-10:] if len(printdata.split("\n")) > 10 else printdata.split("\n")
            for index, line in enumerate(lastlines):
                scr.addstr(index + 2, 0, line)
            scr.refresh()
        except:
            scr = curses.initscr()

    curses.endwin()
    print("\n[+] We're done! Closing the connection.")
    outfile_obj.close()
    writer.close()


asyncio.run(download_from_serial(command, outfile))
