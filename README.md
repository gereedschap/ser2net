# ser2net

ser2net allows you to use serial connections over TCP. A use case is to be able to use python asyncio to talk to a serial connection.

## Usage

ser2net expects a configuration file. The following will make `/dev/ttyUSB0` available on TCP port 8000.

Have the following in `ser2net.yaml` in your current directory.

```yaml
connection: &con00
  accepter: tcp,8000
  connector: serialdev,/dev/ttyUSB0,115200n81,local
```

Then run

```sh
docker run --rm -ti -v "$PWD":/usr/local/etc/ser2net -p 8000:8000 --device=/dev/ttyUSB0 registry.gitlab.com/gereedschap/ser2net ser2net -d
```

You can then give it a try by running `telnet localhost 8000` on the same machine.

