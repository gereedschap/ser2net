FROM ubuntu

RUN apt-get update && apt-get install -y wget gcc python3-dev make libyaml-dev

ARG GENSIO_VER=2.0.1
ARG SER2NET_VER=4.1.6

RUN wget -O gensio.tar.gz https://iweb.dl.sourceforge.net/project/ser2net/ser2net/gensio-$GENSIO_VER.tar.gz && tar -xf gensio.tar.gz
RUN wget -O ser2net.tar.gz https://netcologne.dl.sourceforge.net/project/ser2net/ser2net/ser2net-$SER2NET_VER.tar.gz && tar -xf ser2net.tar.gz

RUN cd gensio-$GENSIO_VER && ./configure && make && make install
RUN cd ser2net-$SER2NET_VER && ./configure && make && make install

RUN apt-get install -y ser2net && apt-get purge -y libwrap0 # This somehow makes it find the gensio shared lib...
